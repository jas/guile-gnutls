;;; GnuTLS --- Guile bindings for GnuTLS.
;;; Copyright (C) 2007-2023 Free Software Foundation, Inc.
;;;
;;; This file is part of Guile-GnuTLS.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Written by Ludovic Courtès <ludo@chbouib.org>.


;;;
;;; Test SRP base64 encoding and decoding.
;;;

(use-modules (gnutls)
             (gnutls build tests))

(define %message
  "GnuTLS is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.")

(run-test
 (lambda ()
   (let ((encoded (srp-base64-encode %message)))
     (and (string? encoded)
          (string=? (srp-base64-decode encoded)
                    %message)))))


;;; arch-tag: ea1534a5-d513-4208-9a75-54bd4710f915
