;;; GnuTLS --- Guile bindings for GnuTLS.
;;; Copyright (C) 2023 Free Software Foundation, Inc.
;;;
;;; This file is part of Guile-GnuTLS.
;;;
;;; This library is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU Lesser General Public
;;; License as published by the Free Software Foundation; either
;;; version 2.1 of the License, or (at your option) any later version.
;;;
;;; This library is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; Lesser General Public License for more details.
;;;
;;; You should have received a copy of the GNU Lesser General Public
;;; License along with this library; if not, see <https://www.gnu.org/licenses/>.

;; Suppose that we have "some-socket", a socket object connected to the
;; client, for instance obtained by the "accept" function.

(let ((server (make-session connection-end/server)))
  (set-session-default-priority! server)

  ;; Request the "anonymous Diffie-Hellman" key exchange method.
  (set-session-priorities! server "NORMAL:+ANON-DH")

  ;; Specify the underlying transport socket.
  (set-session-transport-fd! server (fileno some-socket))

  ;; Create anonymous credentials.
  (let ((cred (make-anonymous-server-credentials))
        (dh-params (make-dh-parameters 1024)))
    ;; Note: DH parameter generation can take some time.
    (set-anonymous-server-dh-parameters! cred dh-params)
    (set-session-credentials! server cred))

  ;; Perform the TLS handshake with the client.
  (handshake server)

  ;; Receive data over the TLS record layer.
  (let ((message (read (session-record-port server))))
    (format #t "received the following message: ~a~%"
            message)

    (bye server close-request/rdwr)))
