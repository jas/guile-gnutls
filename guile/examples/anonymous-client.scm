;;; GnuTLS --- Guile bindings for GnuTLS.
;;; Copyright (C) 2023 Free Software Foundation, Inc.
;;;
;;; This file is part of Guile-GnuTLS.
;;;
;;; This library is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU Lesser General Public
;;; License as published by the Free Software Foundation; either
;;; version 2.1 of the License, or (at your option) any later version.
;;;
;;; This library is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; Lesser General Public License for more details.
;;;
;;; You should have received a copy of the GNU Lesser General Public
;;; License along with this library; if not, see <https://www.gnu.org/licenses/>.

;; Suppose that we have "some-socket", a socket object connected to the
;; server, for instance obtained by the "connect" function.

(let ((client (make-session connection-end/client)))
  ;; Use the default settings.
  (set-session-default-priority! client)

  ;; Request the "anonymous Diffie-Hellman" key exchange method.
  (set-session-priorities! client "NORMAL:+ANON-DH")

  ;; Specify the underlying socket.
  (set-session-transport-fd! client (fileno some-socket))

  ;; Create anonymous credentials.
  (set-session-credentials! client
                            (make-anonymous-client-credentials))

  ;; Perform the TLS handshake with the server.
  (handshake client)

  ;; Send data over the TLS record layer.
  (write "hello, world!" (session-record-port client))

  ;; Terminate the TLS session.
  (bye client close-request/rdwr))
